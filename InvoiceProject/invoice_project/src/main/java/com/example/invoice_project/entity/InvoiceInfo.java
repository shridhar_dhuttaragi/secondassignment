package com.example.invoice_project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int invoice_Id;

    private String bill_to;

    private String invoice_date;
    private String invoice;
    private String due_date;
    private String status;

    private String invoice_note;
    private String filename;
    private String filetype;
    @Lob
    private byte[] fileByte;


    @OneToMany(cascade = CascadeType.PERSIST,mappedBy = "invoiceInfo")
    List<InvoiceDetials> invoice_details=new ArrayList<>();

    private  String grand_total1;

    public InvoiceInfo(String bill_to, String invoice_date, String invoice, String due_date, String status, String grand_total1) {
        this.bill_to = bill_to;
        this.invoice_date = invoice_date;
        this.invoice = invoice;
        this.due_date = due_date;
        this.status = status;
        this.grand_total1 = grand_total1;
    }

    //testing const.
    public InvoiceInfo(int invoice_Id, String bill_to, String invoice_date, String invoice, String due_date, String status, String invoice_note, String filename, String filetype, String grand_total1) {
        this.invoice_Id = invoice_Id;
        this.bill_to = bill_to;
        this.invoice_date = invoice_date;
        this.invoice = invoice;
        this.due_date = due_date;
        this.status = status;
        this.invoice_note = invoice_note;
        this.filename = filename;
        this.filetype = filetype;
        this.grand_total1 = grand_total1;
    }

    public void addDetails(String s, String s1, String s2, String s3, String tax, String inv) {
        this.invoice_details.add(new InvoiceDetials(s,s1,s2,s3,tax,inv,this));
    }
}
