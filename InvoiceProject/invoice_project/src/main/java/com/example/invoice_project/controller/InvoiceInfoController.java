package com.example.invoice_project.controller;

import com.example.invoice_project.entity.InvoiceDetials;
import com.example.invoice_project.entity.InvoiceInfo;
import com.example.invoice_project.repository.InvoiceInfoRepo;
import com.example.invoice_project.service.InvoiceInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@Slf4j
public class InvoiceInfoController {

    @Autowired
    InvoiceInfoService invoiceInfoService;

    @Autowired
    InvoiceInfoRepo invoiceInfoRepo;

//    private static List<String> bill_to;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd ");
    Date date = new Date();
    String date1=formatter.format(date);

//    static {
//        bill_to = new ArrayList<>();
//        bill_to.add("Kiran");
//        bill_to.add("Vicky");
//        bill_to.add("Raju");
//        bill_to.add("Diya");
//    }

    @GetMapping("/")
    public String index()
    {
        return "index";
    }

    @GetMapping(path = {"/invoice"})
    private String getInvoiceForm(Model model) {
//        model.addAttribute("bill_to", bill_to);
        log.info("inside InvoiceForm Controller, here setting the system date and returning invoice_info HTML page.");
        model.addAttribute("invoice_date",date1 );
        return "invoice_information";
    }

    @PostMapping("/saveInvoice1")
    public String saveInvoice1(@ModelAttribute("invoice_information") @Valid InvoiceInfo invoice_information,
                               @ModelAttribute("invoice_details") @Valid InvoiceDetials invoice_details ,
                               @RequestParam("myfile") MultipartFile myfile,HttpServletRequest request) throws IOException {
        log.info("inside the saveInvoice Controller, here collecting all values from <form>.");
        //adding file details to the invoicedetails tbl
        invoice_information.setFilename(myfile.getOriginalFilename());
        invoice_information.setFiletype(myfile.getContentType());
        invoice_information.setFileByte(myfile.getBytes());

        String inv=invoice_information.getInvoice();
        String product[]=request.getParameterValues("product");
        String description[]=request.getParameterValues("description");
        String price[]=request.getParameterValues("price");
        String qty[]=request.getParameterValues("qty");
        String tax[]=request.getParameterValues("tax");
        for(int i=0;i<product.length-1;i++)
        {
                invoice_information.addDetails(product[i],description[i],price[i],qty[i],tax[i],inv);
        }
        System.out.println(invoice_information.getInvoice_details().size());
        invoiceInfoRepo.save(invoice_information);
        return "redirect:/invoicelist";
    }


}
