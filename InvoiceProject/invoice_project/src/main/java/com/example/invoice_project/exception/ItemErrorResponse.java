package com.example.invoice_project.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemErrorResponse {

    private int status;
    private String message;
    private  long timeStamp;
}
