package com.example.invoice_project.repository;

import com.example.invoice_project.entity.InvoiceInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface InvoiceInfoRepo extends JpaRepository<InvoiceInfo,Integer> {

    @Query(value = "select * from INVOICE_INFO where invoice= ?1 ",nativeQuery = true)
    InvoiceInfo findByInvoiceId(String invoice);

    @Modifying
    @Query(value = "DELETE  FROM INVOICE_INFO  where INVOICE = ?1",nativeQuery = true)
    void deleteByInvoiceId(String invoice);

    @Query(value = "SELECT * FROM INVOICE_INFO  ORDER BY due_date ",nativeQuery = true)
    Page<InvoiceInfo> dateFilter(  Pageable pageable);

//    @Query(value = "SELECT * FROM INVOICE_INFO WHERE INVOICE LIKE %:keyword% ",nativeQuery = true)
//    Page<InvoiceInfo> search(String keyword, Pageable pageable);
}
