package com.example.invoice_project.controller;

import com.example.invoice_project.entity.InvoiceInfo;
import com.example.invoice_project.repository.InvoiceInfoRepo;
import com.example.invoice_project.service.InvoiceInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@Slf4j
public class InvoiceListController {

    @Autowired
    InvoiceInfoService invoiceInfoService;

    @Autowired
    InvoiceInfoRepo invoiceInfoRepo;

    @GetMapping(path = "/invoicelist")
    public String  getAllInvoice(Model model) {
        log.info("inside getAllInvoice Controller, here getting all the InvoiceInfo and displaying.");
        return findPaginated(1,   model);

//        ModelAndView mav = new ModelAndView("invoice_info_list");
////        mav.addObject("invoicelists", invoiceInfoRepo.findAll());
//        mav.addObject("invoicelists",invoiceInfoRepo.findAllByFilter());
//        return mav;
    }
    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
                                Model model) {
        int pageSize = 5;
        Page<InvoiceInfo> page = invoiceInfoService.findPaginated(pageNo, pageSize );
        List < InvoiceInfo > listInvoice = page.getContent();
        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("invoicelists", listInvoice);
        return "invoice_info_list";
    }

    @GetMapping("/showForUpdate/{invoice}")
    public String showForUpdate(@PathVariable(value = "invoice") String invoice, Model model) throws Exception {
        log.info("inside showForUpdate Controller, here fetching particular data and given to edit_invoice HTML page. ");
        // get Invoice from the service
        InvoiceInfo invoiceInfo = invoiceInfoService.getInvoiceById(invoice);
        // set Invoice as a model attribute to the form
        model.addAttribute("invoiceInfo", invoiceInfo);
        return "edit_invoice_info";
    }
    @PostMapping("/updateInvoice")
    public String updateInvoice(@ModelAttribute("invoice_information") @Valid InvoiceInfo invoice_information) throws IOException {
        log.info("inside updateInvoice Controller, here updating the data. ");
        String invoice=invoice_information.getInvoice();
        invoiceInfoService.updateInvoice(invoice_information,invoice);
        return "redirect:/invoicelist";
    }
    @RequestMapping("/deleteInvoice/{invoice}")
    public String deleteInvoice(@PathVariable(value = "invoice") String invoice, Model model) {
        log.info("inside deleteInvoice Controller, deleting the Invoice info using invoice.");
        invoiceInfoService.deleteInvoiceById(invoice);
        return "redirect:/invoicelist";
    }

//    @RequestMapping("/searchKeyword")
//    public String searchKeword(Model model, @Param("keyword") String keyword) {
//        log.info("inside Search method in Controller.");
//        return searchfindPaginated(1,   model,keyword);
//    }

//    private String searchfindPaginated(int i, Model model, String keyword) {
//        int pageSize = 5;
//        Page<InvoiceInfo> page = invoiceInfoService.searchfindPaginated(i, pageSize,keyword );
//        List < InvoiceInfo > listInvoice = page.getContent();
//        model.addAttribute("currentPage", i);
//        model.addAttribute("totalPages", page.getTotalPages());
//        model.addAttribute("totalItems", page.getTotalElements());
//        System.out.println(listInvoice.toString());
//        model.addAttribute("invoicelists", listInvoice);
//        return "invoice_info_list";
//    }


}
