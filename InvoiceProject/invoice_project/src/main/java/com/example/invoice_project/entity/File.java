package com.example.invoice_project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data

@NoArgsConstructor
@AllArgsConstructor
public class File {


    String[] products;
    String[] descriptions;
    String[] prices;
    String[] qtys;



}
