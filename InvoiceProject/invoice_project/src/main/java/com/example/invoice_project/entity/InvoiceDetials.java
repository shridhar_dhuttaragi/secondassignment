package com.example.invoice_project.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Entity
@Table
@NoArgsConstructor
@AllArgsConstructor

public class InvoiceDetials {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String inv_no;
    private String product;
    private String description;
    private String price;
    private String qty;
    private String tax;

    @ManyToOne
    InvoiceInfo invoiceInfo;

    public InvoiceDetials(String product, String description, String price, String qty, String tax,String inv_no, InvoiceInfo invoiceInfo) {
        this.product = product;
        this.description = description;
        this.price = price;
        this.qty = qty;
        this.tax = tax;
        this.inv_no=inv_no;
        this.invoiceInfo=invoiceInfo;
    }

    //tetsing cons
    public InvoiceDetials(String s, String mobile, String cell, String s1, String s2, String s3) {
    }


//    public InvoiceDetials(String s, String s1, String s2, String s3, String tax, String inv, InvoiceInfo invoiceInfo) {
//    }
}
