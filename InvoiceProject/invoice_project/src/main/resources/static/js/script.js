
function mySave()
{
    var bill_to=document.getElementById("bill_to").value;
    var invoice_date=document.getElementById("invoice_date").value;
    var invoice=document.getElementById("invoice").value;
//    console.log(invoice);
    var due_date=document.getElementById("due_date").value;
    var status=document.getElementById("status").value;
    var attachment=document.getElementById("myfile").value;

    var table=document.getElementById("data_table");
    var table_len=(table.rows.length)-1;

    var FromDate = new Date(invoice_date);
    var ToDate = new Date(due_date);



    if(bill_to.trim() ==""||bill_to.trim()==null)
    {
        alert("Bill To must be Required.");
        return false;
    }
    else if(invoice.trim() ==""||invoice.trim()==null)
    {
        alert("Invoice must be Required.");
        return false;
    }else if(due_date.trim() ==""||due_date.trim()==null)
    {
        alert("Due Date must be Required.");
        return false;
    }
    else if(status.trim() ==""||status.trim()==null)
    {
        alert("Status must be Required.");
        return false;
    }
    else if(FromDate>ToDate)
    {
        alert(" Due date should greater that Invoice date.");
        return false;
    }
    else if(table_len  ==0||table_len ==null)
    {
        alert("At lest one Invoice Detail isRequired.");
        return false;
    }

    let file_extension=attachment.substring(attachment.lastIndexOf('.') + 1);

    if(file_extension!="")
    {
        if((String(file_extension.toLowerCase())) == "jpg"||(String(file_extension.toLowerCase())) == "png" || (String(file_extension.toLowerCase())) == "jpeg"||(String(file_extension.toLowerCase())) == "excel"||(String(file_extension.toLowerCase())) == "doc"||(String(file_extension.toLowerCase())) == "pdf")
        {
        }else
        { alert("attachment allows only excel/doc/pdf/image type Files.");
        return false;}
    }
}

function add_new_row()
{
var auto_id=document.getElementById("auto_id").value;
var  product=document.getElementById("product").value;
var description=document.getElementById("description").value;
var price=document.getElementById("price").value;
var qty=document.getElementById("qty").value;
var tax=document.getElementById("tax").value;
var total=parseFloat(price)*parseFloat(qty)

if(product ==""||product.trim()==null)
{
     alert("Product must be Required.");
     return false;
}else if(description==""||description.trim()==null)
{
     alert("Description must be Required.");
     return false;
}
else if(price==""||price.trim()==null)
{
     alert("Price must be Required.");
     return false;
}
else if(qty==""||qty.trim()==null)
{
     alert("Qty must be Required.");
     return false;
}
else
{

var table=document.getElementById("data_table");
var table_len=(table.rows.length)-1;
console.log(table_len);
var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'> " +

"<td id='auto_id"+table_len+"'><input type='text' value='"+auto_id+"' class='form-control' readonly='readonly''></td> " +
"<td id='product"+table_len+"'><input type='text'  name='product' value='"+product+"' class='form-control' readonly></td> " +
"<td id='description"+table_len+"'><input type='text' name='description' value='"+description+"' class='form-control' readonly></td> " +
"<td id='price"+table_len+"' ><input type='text' name='price' value='"+price+"' class='form-control' readonly></td> " +
"<td id='qty"+table_len+"' ><input type='text' name='qty' value='"+qty+"' class='form-control' readonly></td> " +
"<td id='tax"+table_len+"' ><input type='text' name='tax' value='"+tax+"' id='taxval"+table_len+"' class='form-control' readonly></td> " +
"<td id='total"+table_len+"' ><input type='text' name='total' value='"+total+"' id='totval"+table_len+"' class='form-control' readonly></td> " +

"<td><i class='fa fa-trash' style='font-size:24px' onclick='delete_row3("+table_len+")'> </i></td> " +
"</tr>";

var sub_total=0;
var total_tax=0;

for(var i=0;i<=table_len;i++)
{

   sub_total+= parseFloat(document.getElementById("totval"+i).value);
   total_tax+=parseFloat(document.getElementById("taxval"+i ).value);
}
document.getElementById("auto_id").value=parseInt(auto_id)+1 ;
document.getElementById("product").value="";
document.getElementById("description").value="";
document.getElementById("price").value="";
document.getElementById("qty").value="";
document.getElementById("tax").value=0;
//document.getElementById("total").value="";
document.getElementById("sub_total").innerHTML=sub_total;
document.getElementById("total_tax").innerHTML=total_tax;
document.getElementById("grand_total1").value=sub_total+total_tax;
}
}

function delete_row3(no)
 {
    var table=document.getElementById("data_table");
    var table_len=(table.rows.length)-1;
    var sub_total= document.getElementById("sub_total").innerHTML;
    var total_tax= document.getElementById("total_tax").innerHTML;
    var grand_total1= document.getElementById("grand_total1").value;
     console.log(sub_total+":"+total_tax+":"+grand_total1);
     for(var i=0;i<=table_len;i++)
         {
         if(no==i){
     //          console.log(document.getElementById("tot"+i).value);
               var particular_tot=parseFloat(document.getElementById("totval"+i).value);
               var particular_tax=parseFloat(document.getElementById("taxval"+i).value)
               var after_deletion_tot=parseFloat(sub_total)-particular_tot;
               var after_deletion_tax=parseFloat(total_tax)-particular_tax;
               console.log(after_deletion_tot+":"+after_deletion_tax);
               document.getElementById("sub_total").innerHTML=after_deletion_tot;
               document.getElementById("total_tax").innerHTML=after_deletion_tax;
               document.getElementById("grand_total1").value=(after_deletion_tax+after_deletion_tot);
         }
         }
          document.getElementById("row"+no+"").outerHTML="";
 }

 function searchFunction() {
     let filter=document.getElementById("keyword").value.toUpperCase();
     var table=document.getElementById("data_table");
     var tr= table.getElementsByTagName('tr');

     for(var i=0;i<tr.length;i++)
     {
         let td=tr[i].getElementsByTagName('td')[2];
         if(td)
         {
             let text=td.innerHTML|| td.textContent;
             if(text.toUpperCase().indexOf(filter)>-1)
             {
                 tr[i].style.display="";
             }else
             {
                 tr[i].style.display="none";
             }
         }
     }
   }









