package com.example.invoice_project.repo;

import com.example.invoice_project.entity.InvoiceDetials;
import com.example.invoice_project.entity.InvoiceInfo;
import com.example.invoice_project.repository.InvoiceDetailsRepo;
import com.example.invoice_project.repository.InvoiceInfoRepo;
import com.example.invoice_project.service.InvoiceInfoService;
import org.junit.Before;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InvoiceInfoRepoTest {

    @Autowired
    InvoiceInfoRepo invoiceInfoRepo;
    @Autowired
    InvoiceDetailsRepo invoiceDetailsRepo;

    @InjectMocks
    InvoiceInfoService invoiceInfoService;

    private InvoiceInfo invoiceInfo;
    private InvoiceDetials invoiceDetials1;
    private InvoiceDetials invoiceDetials2;

    @Test
    @Rollback(value = false)	//here the value will storing into db.
    @Order(1)
    public void saveInvoiceInfo()
    {
        List list=   Arrays.asList(new InvoiceDetials("INV-0111","mobile","cell","100","0","10",invoiceInfo),
                new InvoiceDetials("INV-0111","mobile","cell","100","0","10",invoiceInfo));
        InvoiceInfo invoiceInfo=new InvoiceInfo(1,"sandy","2012-12-23","INV-0111","2012-12-26","save","note","a.png","png",null,list, "1000");
        InvoiceInfo ii=invoiceInfoRepo.save(invoiceInfo);
        assertThat(ii.getInvoice()).isEqualTo("INV-0111");
    }
    @Test
	@Rollback(value = true)
	@Order(2)
	void findByInvoiceId() {
		List list=   Arrays.asList(new InvoiceDetials("INV-0111","mobile","cell","100","0","10",invoiceInfo),
                new InvoiceDetials("INV-0111","mobile","cell","100","0","10",invoiceInfo));
        InvoiceInfo invoiceInfo=new InvoiceInfo(1,"sandy","2012-12-23","INV-0111","2012-12-26","save","note","a.png","png",null,list, "1000");
        InvoiceInfo ii=invoiceInfoRepo.save(invoiceInfo);
		InvoiceInfo ii1=invoiceInfoRepo.findByInvoiceId("INV-0111");
		assertThat(ii1.getInvoice().equals("INV-0111"));
	}
    @Test
	@Rollback(value = false)
	@Order(3)
	public void updateInvoice()
	{  List list=   Arrays.asList(new InvoiceDetials("INV-0111","mobile","cell","100","0","10",invoiceInfo),
            new InvoiceDetials("INV-0111","mobile","cell","100","0","10",invoiceInfo));
        InvoiceInfo invoiceInfo=new InvoiceInfo(1,"sandy","2012-12-23","INV-0111","2012-12-26","save","note","a.png","png",null,list, "1000");
        InvoiceInfo ii=invoiceInfoRepo.save(invoiceInfo);
        ii.setBill_to("sandy111");
		InvoiceInfo ii1=invoiceInfoRepo.findByInvoiceId("INV-0111");
		assertThat(ii1).isEqualTo(ii);
	}

    @Test
	@Rollback(value = false)
	@Order(4)
	public void deleteByInvoiceId()
	{
        List list=   Arrays.asList(new InvoiceDetials("INV-0111","mobile","cell","100","0","10",invoiceInfo),
                new InvoiceDetials("INV-0111","mobile","cell","100","0","10",invoiceInfo));
        InvoiceInfo invoiceInfo=new InvoiceInfo(1,"sandy","2012-12-23","INV-0111","2012-12-26","save","note","a.png","png",null,list, "1000");
        InvoiceInfo ii=invoiceInfoRepo.save(invoiceInfo);
		invoiceInfoRepo.deleteByInvoiceId("INV-0111");
		invoiceDetailsRepo.deleteByInvoiceId("INV-0111");
		assertThat(invoiceInfoRepo.findByInvoiceId("INV-0111")).isNotEqualTo(invoiceInfo);
	}

}