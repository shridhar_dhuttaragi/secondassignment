$(document).ready(function(){
         $('#invitees').multiselect({
          nonSelectedText: 'Select Framework',
         });
        });
        function myMeetingSubmit()
        {
            var title=document.getElementById("title").value;
            var color=document.getElementById("color").value;
            var start=document.getElementById("start").value;
            var end=document.getElementById("end").value;
            var location=document.getElementById("location").value;
            var invitees=document.getElementById("invitees").value;
            var url=document.getElementById("url").value;

            var FromDate = new Date(start);
            var ToDate = new Date(end);

<!--            console.log(FromDate+":"+ToDate);-->
<!--            console.log(FromDate.getTime()+":"+ToDate.getTime());-->
<!--            alert(FromDate>ToDate)-->
<!--            alert(FromDate.getTime()>ToDate.getTime());-->

             if( title=="" || title==null)
            {
                alert("Meeting Title is Required.");
                return false;
            }
            else if(color==""||color==null)
            {
                alert("Event Color is Required.");
                return false;
            }
            else if(start==""||start==null)
            {
                alert("Start Date is Required.");
                return false;
            }
            else if(end==""||end==null)
            {
                alert("End Date is Required.");
                return false;
            }
            else if(FromDate.getTime()>ToDate.getTime() && FromDate>ToDate)
            {
                alert(" End Date should greater that Start Date.");
                return false;
            }
            else if(location==""||location==null)
            {
                alert("Location is Required.");
                return false;
            }
             else if(invitees==""||invitees==null)
            {
                alert("Invitees is Required.");
                return false;
            }
            else if(url==""||url==null)
            {
                alert("Meeting url is Required.");
                return false;
            }else if(! url.startsWith("https://"))
                {
                    alert("Enter valid Url ex:https://______");
                    return false;
                }
        }