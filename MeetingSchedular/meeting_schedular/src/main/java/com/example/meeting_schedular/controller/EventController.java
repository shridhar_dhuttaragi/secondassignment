package com.example.meeting_schedular.controller;

import com.example.meeting_schedular.entity.Event;
import com.example.meeting_schedular.repository.EventRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Controller
public class EventController {
    @Autowired
    EventRepo eventRepo;


    @RequestMapping(value="/cal", method= RequestMethod.GET)
    public ModelAndView cal() {
        ModelAndView modelAndView = new ModelAndView("view_calendar");
        return modelAndView;
    }
    @RequestMapping(value="/allevents1", method= RequestMethod.GET)
    @ResponseBody
    public List<Event> event() {
        List<Event> list= eventRepo.findAll();
        return list;
    }

}
