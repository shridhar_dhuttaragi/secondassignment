package com.example.meeting_schedular.repository;

import com.example.meeting_schedular.entity.Meeting_Info;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface Meeting_Info_Repo extends JpaRepository<Meeting_Info,Integer> {

    @Query(value = "SELECT * FROM meeting_info WHERE emp_id=?1",nativeQuery = true)
    List<Meeting_Info> findByEmpId(int emp_id);

    @Modifying
    @Query(value = "DELETE MEETING_INFO WHERE EMP_ID=?1",nativeQuery = true)
    void deleteByEmpId(int e_id);
}
