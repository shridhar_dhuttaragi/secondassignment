package com.example.meeting_schedular.controller;

import com.example.meeting_schedular.entity.Employee_Info;
import com.example.meeting_schedular.service.Employee_Info_Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@Slf4j
public class Employee_Info_Controller {

    @Autowired
    Employee_Info_Service employee_info_service;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd ");
    Date date = new Date();
    String date1=formatter.format(date);

    @GetMapping("/")
    public String index()
    {
        return "index";
    }

    @GetMapping(path = {"/employeeinfo"})
    private String getEmployeeForm(Model model) {
        log.info("inside getEmployeeForm Controller, returning Employee_Info HTML page.");
        model.addAttribute("invoice_date",date1 );
        return "employee_info";
    }
    @PostMapping(path = {"/saveEmployee"})
    private String saveEmployee(@ModelAttribute("employeeinfo") Employee_Info employee_info, Model model) {
        log.info("inside saveEmployee Controller.");
        employee_info_service.saveEmployee(employee_info);
        return "redirect:/employeelist";
    }
}
