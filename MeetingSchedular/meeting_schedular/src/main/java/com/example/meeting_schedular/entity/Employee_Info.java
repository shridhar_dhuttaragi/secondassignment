package com.example.meeting_schedular.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee_Info {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int e_id;
    private String e_firstname;
    private String e_lastname;
    private String e_mail;
    private String e_phone;
    private String e_designation;
    private String e_employment_type;
    private String e_department;

}
