package com.example.meeting_schedular.service;

import com.example.meeting_schedular.entity.Meeting_Info;
import com.example.meeting_schedular.repository.Meeting_Info_Repo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class Meeting_Info_Service {


    @Autowired
    Meeting_Info_Repo meeting_info_repo;

    public Meeting_Info saveMeetingInfo(Meeting_Info meeting_info) {
        log.info("meeting info saved.");
        return meeting_info_repo.save(meeting_info);
    }

    public Meeting_Info findById(int meeting_id) {
        log.info("find meeting service.");
        return meeting_info_repo.findById(meeting_id).get();
    }


    public Meeting_Info updateMeetingInfo(Meeting_Info meeting_info, int meeting_id) {
        log.info("update meeting info");
        Meeting_Info meeting_info1 = meeting_info_repo.findById(meeting_id).get();
        meeting_info1.setTitle(meeting_info.getTitle());
        meeting_info1.setColor(meeting_info.getColor());
        meeting_info1.setStart(meeting_info.getStart());
        meeting_info1.setEnd(meeting_info.getEnd());
        meeting_info1.setLocation(meeting_info.getLocation());
        meeting_info1.setInvitees(meeting_info.getInvitees());
        meeting_info1.setUrl(meeting_info.getUrl());
        return meeting_info_repo.save(meeting_info1);
    }

    public void deleteMeeting(int meeting_id) {
        log.info("delete meeting service");
        meeting_info_repo.deleteById(meeting_id);
    }


    public List<Meeting_Info> findAll() {
        log.info("getting all meeting Info.");
        return meeting_info_repo.findAll();
    }
}
