package com.example.meeting_schedular.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Meeting_Info {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int meeting_id;
    private String emp_id;
    private String title;
    private String color;
    private String start;
    private String end;
    private String location;
    private String invitees;
    private String url;

}
