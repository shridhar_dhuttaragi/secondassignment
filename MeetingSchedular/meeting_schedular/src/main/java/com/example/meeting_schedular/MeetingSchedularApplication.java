package com.example.meeting_schedular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeetingSchedularApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeetingSchedularApplication.class, args);
	}

}
