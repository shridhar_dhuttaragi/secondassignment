package com.example.meeting_schedular.repository;

import com.example.meeting_schedular.entity.Employee_Info;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Employee_Info_Repo extends JpaRepository<Employee_Info,Integer> {
}
