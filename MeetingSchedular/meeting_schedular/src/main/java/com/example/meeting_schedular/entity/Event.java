package com.example.meeting_schedular.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    private String title;
    private String description;
    private String start;
    @Column(name = "end")
    private String finish;

    ArrayList list=new ArrayList();

    public Event(String meetingtitle, String startdate, String enddate) {
        this.title=meetingtitle;
        this.start=startdate;
        this.finish=enddate;
    }

    public void addDetails(String meetingtitle, String startdate, String enddate) {
        list.add(new Event(meetingtitle,startdate,enddate));
    }
}
