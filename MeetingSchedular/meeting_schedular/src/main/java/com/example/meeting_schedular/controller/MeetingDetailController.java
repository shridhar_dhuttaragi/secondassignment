package com.example.meeting_schedular.controller;


import com.example.meeting_schedular.entity.Meeting_Info;
import com.example.meeting_schedular.repository.EventRepo;
import com.example.meeting_schedular.repository.Meeting_Info_Repo;
import com.example.meeting_schedular.service.Meeting_Info_Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@Slf4j
public class MeetingDetailController {

    @Autowired
    Meeting_Info_Service meeting_info_service;
    @Autowired
    Meeting_Info_Repo meeting_info_repo;
    @Autowired
    EventRepo eventRepo;
    static  int id;

    @GetMapping("/viewCalendar/{e_id}")
    public ModelAndView viewCalendar(@PathVariable(value = "e_id") int e_id, Model model) throws Exception {
        log.info("inside viewCalendar Controller,displaying the calender.");
        ModelAndView mav = new ModelAndView("view_calendar");
        id=e_id;
        mav.addObject("displaycalendar");
        return mav;
    }

    @RequestMapping(value = "/allevents", method= RequestMethod.GET)
    @ResponseBody
    public List<Meeting_Info> allMeeting()  {
        log.info("all events");
//        System.out.println(id);
       List<Meeting_Info> list= meeting_info_repo.findByEmpId(id);
       return list;
    }

    @GetMapping("/saveMeeting/{e_id}")
    public ModelAndView saveMeeting(@PathVariable(value = "e_id") int e_id, Model model) throws Exception {
        log.info("inside saveMeeting Controller,to save d meeting.");
//        httpSession.setAttribute("e_id",e_id);
        ModelAndView mav = new ModelAndView("meeting_info");
        model.addAttribute("e_id",e_id);
        return mav;
    }
    @PostMapping("/savemeetingInfo")
    private String savemeetingInfo(@ModelAttribute("meetinginfo")Meeting_Info meeting_info, Model model) {
        log.info("inside savemeetingInfo Controller.");
        meeting_info_service.saveMeetingInfo(meeting_info);
        return "redirect:/employeelist";
    }


    @GetMapping("/listoutMeeting/{emp_id}")
    public ModelAndView editMeeting(@PathVariable(value = "emp_id") int emp_id, Model model) throws Exception {
        log.info("inside listoutMeeting Controller,"+emp_id);
        List<Meeting_Info> meeting_info=meeting_info_repo.findByEmpId(emp_id);
        ModelAndView mav = new ModelAndView("listout_meeting");
//        System.out.println(meeting_info.toString());
        mav.addObject("listout_meeting",meeting_info);
        return mav;
    }

    @GetMapping("/editMeeting/{meeting_id}")
    public ModelAndView showEditMeeting(@PathVariable(value = "meeting_id") int meeting_id, Model model) throws Exception {
        log.info("inside showEditMeeting Controller,to edit d meeting."+meeting_id);
//        Meeting_Info meeting_info= meeting_info_repo.findById(meeting_id).get();
        ModelAndView mav = new ModelAndView("edit_meeting_info");
        mav.addObject("editmeetinginfo",meeting_info_service.findById(meeting_id));
        return mav;
    }
    @PostMapping("/saveMeetingInfo")
    public String saveMeetingInfo(@ModelAttribute("editedmeetinginfo")Meeting_Info meeting_info, Model model)
    {
        log.info("inside saveMeetingInfo Controller,");
        int meeting_id=meeting_info.getMeeting_id();
        meeting_info_service.updateMeetingInfo(meeting_info,meeting_id);
        return "redirect:/employeelist";
    }

    @RequestMapping("/deleteMeeting/{meeting_id}")
    public String deleteMeeting(@PathVariable("meeting_id")int meeting_id)
    {
        log.info("inside deleteMeeting Controller,"+meeting_id);
        meeting_info_service.deleteMeeting(meeting_id);
        return "redirect:/employeelist";
    }
}
