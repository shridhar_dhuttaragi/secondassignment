package com.example.meeting_schedular.service;

import com.example.meeting_schedular.entity.Employee_Info;
import com.example.meeting_schedular.exception.ItemNotFoundException;
import com.example.meeting_schedular.repository.Employee_Info_Repo;
import com.example.meeting_schedular.repository.Meeting_Info_Repo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j

public class Employee_Info_Service {

    @Autowired
    Employee_Info_Repo employee_info_repo;
    @Autowired
    Meeting_Info_Repo meeting_info_repo;


    public Employee_Info saveEmployee(Employee_Info employee_info) {
        log.info("inside saveEmployee Service,");
        return employee_info_repo.save(employee_info);
//         employee_info;
    }

    public Page<Employee_Info> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return this.employee_info_repo.findAll(pageable);
    }

    public Employee_Info getEmployeeById(int e_id) throws ItemNotFoundException {
//       return employee_info_repo.findById(e_id);
        Optional<Employee_Info> employee_info = employee_info_repo.findById(e_id);
        Employee_Info employee_info1 = null;
        if (employee_info.isPresent()) {
            employee_info1 = employee_info.get();
        } else {
            throw new ItemNotFoundException("Employee not Found");
        }
        return employee_info1;
    }

    public Employee_Info updateEmployee(Employee_Info employee_info) {
        log.info("inside updateEmployee Service,");
        int e_id = employee_info.getE_id();
        Employee_Info employee_info1 = employee_info_repo.findById(e_id).get();

        employee_info1.setE_firstname(employee_info.getE_firstname());
        employee_info1.setE_lastname(employee_info.getE_lastname());
        employee_info1.setE_mail(employee_info.getE_mail());
        employee_info1.setE_phone(employee_info.getE_phone());
        employee_info1.setE_designation(employee_info.getE_designation());
        employee_info1.setE_department(employee_info.getE_department());
        employee_info1.setE_employment_type(employee_info.getE_employment_type());
        return employee_info_repo.save(employee_info1);
    }

    public void deleteEmployeeById(int e_id) {
        log.info("inside deleteEmployee Service,");
        employee_info_repo.deleteById(e_id);
        meeting_info_repo.deleteByEmpId(e_id);

    }

}
