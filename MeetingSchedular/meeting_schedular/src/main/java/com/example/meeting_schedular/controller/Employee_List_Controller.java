package com.example.meeting_schedular.controller;

import com.example.meeting_schedular.entity.Employee_Info;
import com.example.meeting_schedular.repository.Employee_Info_Repo;
import com.example.meeting_schedular.service.Employee_Info_Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;


@Controller
@Slf4j
public class Employee_List_Controller {

    @Autowired
    Employee_Info_Repo employee_info_repo;
    @Autowired
    Employee_Info_Service employee_info_service;

    @GetMapping(path = {"/employeelist"})
    private String getEmployeeListForm(Model model) {
        log.info("inside getEmployeeListForm Controller, returning employee_list HTML page.");
        return findPaginated(1,   model);

    }
    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
                                Model model) {
        int pageSize = 5;
        Page<Employee_Info> page = employee_info_service.findPaginated(pageNo, pageSize );
        List< Employee_Info > listInvoice = page.getContent();
        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("employeelists", listInvoice);
        return "employee_list";
    }
    @GetMapping("/showEmployeeUpdate/{e_id}")
    public String showForUpdate(@PathVariable(value = "e_id") int e_id, Model model) throws Exception {
        log.info("inside showEmployeeUpdate Controller, here fetching particular data and given to edit_Employee HTML page. ");
         Employee_Info employee_info = employee_info_service.getEmployeeById(e_id);
        model.addAttribute("employeelists", employee_info);
        return "edit_employee_info";
    }
    @PostMapping("/updateEmployee")
    public String updateEmployee(@ModelAttribute("employeeinfo") Employee_Info employee_info) throws Exception {
        log.info("inside updateEmployee Controller, here updating d details of tht selected Employee.");
        int sid=employee_info.getE_id();
        log.info(String.valueOf(sid));
        employee_info_service.updateEmployee(employee_info);
        return "redirect:/employeelist";
    }
    @RequestMapping("/deleteEmployee/{e_id}")
    public String deleteInvoice(@PathVariable(value = "e_id") int e_id, Model model) {
        log.info("inside deleteEmployee Controller, deleting the Employee.");
        employee_info_service.deleteEmployeeById(e_id);
        return "redirect:/employeelist";
    }

    @GetMapping("/viewCalendar")
    public ModelAndView viewCalendar(@PathVariable(value = "e_id") int e_id, Model model) throws Exception {
        log.info("inside viewCalendar Controller,displaying the calender.");
        ModelAndView mav = new ModelAndView("view_calendar");
        mav.addObject("displaycalendar");
        return mav;
    }



}
