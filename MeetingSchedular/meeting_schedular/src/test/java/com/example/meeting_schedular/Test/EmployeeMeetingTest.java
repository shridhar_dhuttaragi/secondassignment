package com.example.meeting_schedular.Test;

import com.example.meeting_schedular.entity.Employee_Info;
import com.example.meeting_schedular.entity.Meeting_Info;
import com.example.meeting_schedular.exception.ItemNotFoundException;
import com.example.meeting_schedular.repository.Employee_Info_Repo;
import com.example.meeting_schedular.repository.Meeting_Info_Repo;
import com.example.meeting_schedular.service.Employee_Info_Service;
import com.example.meeting_schedular.service.Meeting_Info_Service;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
//@DataJpaTest
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
 class EmployeeMeetingTest {
//    @MockBean
//    Employee_Info_Repo employee_info_repo;
//    @Autowired
//    Meeting_Info_Repo meeting_info_repo;
    @Autowired
    Employee_Info_Service employee_info_service;
    @Autowired
    Meeting_Info_Service meeting_info_service;

    @Test
    @Order(1)
    @Rollback(value = false)
    public void saveEmployeeTest()
    {
       Employee_Info employee_info=new Employee_Info(1,"sandy","d","s@gmail.com","9012239877","TL","part-time","Cloud");
       Employee_Info ei=employee_info_service.saveEmployee(employee_info);
       assertThat(ei).isEqualTo(employee_info);
//         Meeting_Info meeting_info1=new Meeting_Info(11,"1","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
//         Meeting_Info meeting_info2=new Meeting_Info(12,"1","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
    }
   @Test
   @Order(2)
   @Rollback(value = false)
   public void getEmployeeByIdTest() throws ItemNotFoundException {
      Employee_Info employee_info=new Employee_Info(1,"sandy","d","s@gmail.com","9012239877","TL","part-time","Cloud");
      Employee_Info ei=employee_info_service.saveEmployee(employee_info);
      Employee_Info ei1=employee_info_service.getEmployeeById(1);
      assertThat(ei).isEqualTo(ei1);
   }
   @Test
   @Order(3)
   @Rollback(value = false)
   public void updateEmployeeTest() throws ItemNotFoundException {
      Employee_Info employee_info=new Employee_Info(1,"sandy","d","s@gmail.com","9012239877","TL","part-time","Cloud");
      Employee_Info ei=employee_info_service.saveEmployee(employee_info);
      ei.setE_firstname("sandy111");
      Employee_Info ei1=employee_info_service.updateEmployee(ei);
      assertThat(ei).isEqualTo(ei1);
   }
   @Test
   @Order(4)
   @Rollback(value = false)
   public void deleteEmployeeByIdTest() throws ItemNotFoundException {
      Employee_Info employee_info=new Employee_Info(1,"sandy","d","s@gmail.com","9012239877","TL","part-time","Cloud");
      Employee_Info ei=employee_info_service.saveEmployee(employee_info);
      employee_info_service.deleteEmployeeById(ei.getE_id());
//      assertThat(employee_info_repo.existsById(1)).isFalse();
   }

   @Test
   @Order(5)
   @Rollback(value = false)
   public void saveMeetingTest()
   {
      Meeting_Info meeting_info1=new Meeting_Info(1,"11","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
      Meeting_Info mi=meeting_info_service.saveMeetingInfo(meeting_info1);
      assertThat(mi).isEqualTo(meeting_info1);
   }
    @Test
    @Order(6)
    @Rollback(value = false)
    public void getMeetingByIdTest() throws ItemNotFoundException {
        Meeting_Info meeting_info1=new Meeting_Info(1,"11","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
        Meeting_Info mi=meeting_info_service.saveMeetingInfo(meeting_info1);
        Meeting_Info mi1=meeting_info_service.findById(1);
        assertThat(mi).isEqualTo(mi1);
    }
    @Test
    @Order(7)
    @Rollback(value = false)
    public void updateMeetingTest()   {
        Meeting_Info meeting_info1=new Meeting_Info(1,"11","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
        Meeting_Info mi=meeting_info_service.saveMeetingInfo(meeting_info1);
        mi.setTitle("Lunch");
        Meeting_Info mi1=meeting_info_service.updateMeetingInfo(mi,1);
        assertThat(mi).isEqualTo(mi1);
    }
    @Test
    @Order(8)
    @Rollback(value = false)
    public void deleteMeetingTest()   {
        Meeting_Info meeting_info1=new Meeting_Info(1,"11","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
        Meeting_Info mi=meeting_info_service.saveMeetingInfo(meeting_info1);
        meeting_info_service.deleteMeeting(1);
//        assertThat(mi).isEqualTo(mi1);
    }
}
