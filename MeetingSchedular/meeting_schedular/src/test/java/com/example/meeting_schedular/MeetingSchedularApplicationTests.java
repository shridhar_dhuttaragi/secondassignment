package com.example.meeting_schedular;

import com.example.meeting_schedular.entity.Employee_Info;
import com.example.meeting_schedular.entity.Meeting_Info;
import com.example.meeting_schedular.exception.ItemNotFoundException;
import com.example.meeting_schedular.repository.Employee_Info_Repo;
import com.example.meeting_schedular.repository.Meeting_Info_Repo;
import com.example.meeting_schedular.service.Employee_Info_Service;
import com.example.meeting_schedular.service.Meeting_Info_Service;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@SpringBootTest
class MeetingSchedularApplicationTests {

    @Test
    void contextLoads() {
    }

//	@Test
//	@Order(5)
//	public void saveMeetingInfoTest()
//	{
//		Meeting_Info meeting_info=new Meeting_Info(11,"1","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
//		doReturn(meeting_info).when(meeting_info_repo).save(any());
//		Meeting_Info employee_info1=meeting_info_service.saveMeetingInfo(meeting_info);
//	}
//
//	@Test
//	@Order(6)
//	public  void updateMeetingTest()
//	{
//
//	}
//	@Test
//	@Order(7)
//	public  void findMeetingByIdTest()
//	{
//		Meeting_Info meeting_info=new Meeting_Info(11,"1","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
//		doReturn(Optional.of(meeting_info)).when(meeting_info_repo).findById(11);
//		Meeting_Info returnedWidget = meeting_info_service.findById(11);
//	}
//
//	@Test
//	@Order(8)
//	public void deleteMeeting()
//	{
//		Meeting_Info meeting_info=new Meeting_Info(11,"1","zooom","#000","2021-12-25T18:10","2021-12-25T18:50","BGK","sd","https://ds");
//		doReturn(Optional.of(meeting_info)).when(meeting_info_repo).findById(11);
//		meeting_info_repo.deleteById(11);
//	}

}
